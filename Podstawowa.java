
public class Podstawowa {

	public static void main(String[] args) {
		//tu deklarujemy zmienną, którą będziemy gdzieś w programie wykorzystywać. 
		//Java nie wymaga ustaiwania domyślnych wartości (sama powinna zerować wartosć zmiennej)
		//ale dobra prkatyka programistyczna mówi, że ZAWSZE powinno się podawać deklarowanym zmiennym
		//domyślną wartość.
		int znak = 0;
		
		System.out.print("Witaj, podaje jeden ze znaków by zobaczyć jego postać liczbową: ");
		
		try {
			//tutaj używamy już zmiennej wcześniej zadeklarowanej. Mamy do niej dostęp,
			//i jeżeli kod try przebiegnie bez problemów to zmienna zachowa wartość pobraną przez
			//konsolę od użytkownika
			//Poniższa instrukcja pozwala na pobranie od użytkonika JENDEGO BAJTU DANYCH. Wartość podana z klawiatury (będzie 
			//to jeden ze znaków ASCII bądz część znaku UTF) zostanie zamieniona na wartość integer. 
			znak = System.in.read();
			
		}
		catch(Exception blad) {
			//ponieważ wcześniej kod był pisany w Cp1250 (Windows-1250) i poprzez właściwości pliku została zamieniona
			//strona kodowania tekstu na UTF-8, litery ze znakami narodowymi zamieniły się w nieczytelne znaczki
			//ponieważ tego typu znaki znajdują się pod liczbami, które wcześniej zostały przypisane jeszcze w Cp1250
			System.err.println("Wyst�pi� b��d wczytywania!");
			System.err.println("Dane o b��dzie " + blad.getMessage());
		}
		//wyświetlamy wartość pobranego bajdu oraz jego postać jako znaku w komputerze (powinien mieć ppstać bo zakładamy,
		//że znak został wprowadzony przez standardową klawiaturę). 
		System.out.print((char)znak + " to w kodzie ASCII " + znak);
	}

}
