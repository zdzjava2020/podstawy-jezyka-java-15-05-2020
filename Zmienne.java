
public class Zmienne {

	public static void main(String[] args) {
		
		//typ zbliżony do char oraz int; tak jak char przyjmuje tylko 1 bajt danych, zaś tak jak int wyświetla
		//dane w postaci liczbowej dziesiętnej.
		byte bajt = 'C';
		
		//zamiast pisać SZTYWNĄ ilość zmiennych przechowujących dane możemy...
		short polski_ocena_1 = 5;
		short polski_ocena_2 = 3;
		///....
		
		short polski_ocena_10 =4;
		
		//zapisać je pod jedną nazwą wyykorzystując zmienne tablicowe!
		//powyższa seria once z polskiego mogłaby wyglądać następująco:
		short polski_oceny[] = {5,			3,			4};
			//INDEKSY WARTOŚCI:  0			1			2
		//tablice kolejno dodawane do nich wartośc zapisują JEDNA PO DRUGIEJ; oczywiścike tyczy się to wypadku powyżej
		//w innych wypadkach możemy indeksami dysponować dowolnie, np:
		
		short matematyka_oceny[] = new short[50];
		
		//powyższa deklaracja zmiennej tablicowej nie będzie zawierać domyślnych wartości. Za to utoworzymy tablicę, 
		//która będzie mogła przechowować DO 50 WARTOŚCI (50 ocen z matematyki)
		
		//Trzeba pamiętać, że tablice są numerowane od 0 do wartości podanej przez programistę - 1(czyli efektywnie od 
		//0 do 49 w powyższym przykładzie)
		
		//wcześniej zadeklarowana tablica pozwala nam na dodawanie wartości w dowolnym indeksie tablicy matematyka 
		//(oczywiście w zakresie 0-49)
		matematyka_oceny[2] = 6;
		
		//to zadziała niepoprawnie!
		//matematyka_oceny[50] = 3; //<- indeks zmiennej poza zakresem!
		
		//to też nie zadziała -> chcemy dopisać wartość do tablicy, której wielkość (ilość zmiennych) została niejawnie 
		//ustawiona na 3
		//polski_oceny[3] = 6; //<- ten sam błąd co powyżej!
		
		//kontrolnie wyświetlamy wartość modyfikowanego indeksu celem sprawdzenia, czy wartość zostanie podmieniona
		System.out.println(polski_oceny[1]);
		
		//to natomiast zadziała i wartość spod indeksu 1 zostanie zamieniona:
		polski_oceny[1]=6;
		
		System.out.println("Wartość pojedynczego bajtu: " + bajt);
		System.out.println("Oceny z polskiego: " + polski_oceny[0]+","+polski_oceny[1]+","+polski_oceny[2]);
	}

}
