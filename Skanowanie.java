import java.util.Scanner;


public class Skanowanie {
	public static void main(String[] args) {
		boolean pelnoletni=false;
		double liczba= .0; // <- ten zapis jest równoważny zapisowi 0.0
		String imie = "", 
				zwierzak = "", 
				data_urodzenia= "";
		
		System.out.print("Podaj swoje imię: ");
		
		Scanner wejscie = new Scanner(System.in);
		
		imie = wejscie.nextLine();
		
		System.out.print("Czy jesteś pełnoletni: ");
		
		pelnoletni=wejscie.nextBoolean();
		
		System.out.print("Jaka jest Twoja ulubiona liczba: ");
		
		try {
			liczba=wejscie.nextDouble();
		}
		catch (Exception e) {
			
			System.err.println("Kod błędu: ");
			e.printStackTrace();
			wejscie.next();
		}
		
		System.out.print("Podaj imię ulubionego zwierzaka: " );
		
		zwierzak=wejscie.next();
		
		wejscie.close();
		
		System.out.println("Twoje imię to: " + imie + ". Jesteś " + pelnoletni+", a Twoja ulubiona liczba to: " + liczba);
		
		//Powyżej mamy do czynienia ze złączaniem ciągów znakowych. 
		//Zakładamy, że użytkownik podał następujące dane: Piotr, true, 78
		//Pierwszym ciągiem znakowym jest "Twoje imię to: "
		//Drugim ciągiem znakowym jest zmienna o nazwie imie
		//W tym momencie operator + działa tutaj jako łącznik dwóch ciągów znakowych, nie zaś jako sumator wartości
		//Przez to mamy ciąg "Twoje imię to: Piotr"
		//Trzeci ciąg to ". Jesteś". Tutaj również następuje złączenie
		//Czwraty element NIE JEST ciągiem znakowym!. Java rozpoznaje typy danych, w związku z czym powinna zgłosić
		//błąd składni, ale tego nie ROBI. DLACZEGO? NIEJAWNA KONWERSJA - bez "wiedzy" programisty
		//Piąty ciąg to ", a Twoja ulubiona liczba to: "
		//Szósty ciąg to znowu nie ciąg! Znowu konwersja niejawna
		
	}
}
