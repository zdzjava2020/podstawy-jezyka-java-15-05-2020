import java.io.IOException;

public class Wczytywanie {

	public static void main(String[] args) {
		//poprzednia deklaracja
		//byte imie[] = new byte[10];
		byte imie[] = null;
		
		System.out.print("Witaj, podaj swoje imię: ");
		
		
		try {
			byte bufor[] = new byte[8128];
			System.in.read(bufor);
			System.out.println("Wejście: " + System.in.available());
			imie=new byte[System.in.available()];
			imie=bufor;
			//poniższy kod wczytuje kolejno znaki z konsoli do zmiennej imie, ale nie wczyta więcej niż 10 znaków
			//wszystkie niedoczytane znaki po prostu zostaną utracone (nie wczyta więcej niż 10 ponieważ tyle ma zadeklarowane
			//tablica (tablica imię rzecz jasna)
			//System.in.read(imie);
			
			//ta metoda z kolei będzie czytać tyle znaków ile podaliśmy jej jako parametr (ww tym wypadku musi zostać wczytane
			//10 znaków, w zasadzie bajtów)
			//imie = System.in.readNBytes(10);
			
			//ta funckja nie zkończy się do chwili, kiedy będą jakiekolwiek dane do przeczytania. Bez przerwania jej bądz
			//implementacji włąsnego strumienia może nie działać tak, jak tego oczekujemy
			//imie=System.in.readAllBytes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Twoje imię to: " + new String(imie) + ".");
	}

}
